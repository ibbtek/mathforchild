/*
 * The MIT License
 *
 * Math4Child
 *
 * Copyright 2015 Ibbtek <http://ibbtek.altervista.org/>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.ibbtek.utilities;

import java.awt.Toolkit;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/**
 *
 * @author Ibbtekware
 */
public class JTextLimited extends PlainDocument {
    private int max=0;
    public JTextLimited(int max){
        super();
        this.max=max;
    }
    @Override
    public void insertString
                (int offset, String str, AttributeSet attr) throws
                BadLocationException
        {
        if ((getLength() + str.length()) > max){
        Toolkit.getDefaultToolkit().beep();//joue un petit son
        return;
        }
        if(str.equals("-")==true && getLength()==0)
            super.insertString(offset, str, attr);
        else{
            try{
                Integer.parseInt(str);
                super.insertString(offset, str, attr);

            }catch(NumberFormatException | BadLocationException e){
                //..On ne fait rien
            }
        }
    }
}
